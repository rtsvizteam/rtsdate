

spl = function(s, delim = ',') unlist(strsplit(s,delim))



###############################################################################
#' Custom Date function
#' 
#' Custom syntax to specify **relative** date ends. The expression given to these functions must
#' be in the following format:
#' ([identifier] [periodicity]) [in/every/of] ([identifier] [periodicity]) ... [in/every/of] ([identifier] [periodicity])
#' 
#' Identifier can be:
#' * last
#' * first
#' * 1st, 2nd, 3rd, and etc
#' * 1st to last, 2nd to last, 3rd to last, and etc
#' 
#' Periodicity can be:
#' * day or Mon, Tue, Wed, and etc
#' * week
#' * month or M (all months) or Jan, Feb, Mar and etc, or M1 (same as Jan), M2 (same as Feb)
#' * quarter or Q (all quarters) or Q1, Q2, Q3, Q4
#' * year
#' 
#' For example:
#' * last day in Apr
#' * last day in first week in Apr
#' * 3rd Mon in last M in Q1
#' * 10th to last day in Apr
#' 
#' Sample logic:
#' "last day in first week in Apr"
#' Execution plan:
#' 1. split monthly and pick Apr
#' 2. split weekly and pick first
#' 3. split daily and find last day
#'
#' @param expr expression that describes the dates to be extracted
#' @param dates dates
#' @param keep.outside flag to keep matches that fall outside of selection.
#'		For example, 5th Mon in Apr will match last Mon in Apr if 5th 
#'		Monday is not present.
#'
#' @return list with days.since and days.till arrays
#'
#' @examples
#' \dontrun{ 
#' dates = seq(Sys.Date()-1000, Sys.Date(), 1)
#' custom.date('last day in Apr', dates)
#' }
#' @export 
###############################################################################
custom.date = function(expr, dates, keep.outside=TRUE) {
	# split tokens +/-
	# + add dates
	# - remove dates
	tokens = spl(expr, '\\-')
	if(len(tokens)>1) {
		dummy = rep('-',len(tokens))
		dummy[1] = ''
		tokens = paste(dummy, tokens, sep='')
	}
	tokens = spl(tokens,'\\+')
	if(len(tokens) == 1)
		return(custom.date.base(expr, dates, keep.outside))
	
	# store selected 
	index = rep(F, len(dates))
	
	# sort: first add; next remove
	tokens = names(sort(vapply(tokens,substr,character(1),1,1) == '-'))
		
	for(token in tokens)
		if(substr(token,1,1) == '-')
			index[custom.date.base(substr(token,2,10000), dates, keep.outside)] =  FALSE		 
		else
			index[custom.date.base(token, dates, keep.outside)] =  TRUE

	(1:len(dates))[index]
}


custom.date.base = function(expr, dates, keep.outside=TRUE) {
	dates = as.Date(dates)
	
	# split tokens with *in*, *every*, *of*, *of every*, *in every*
	expr = gsub('the ', '', tolower(expr))
	expr = gsub(',', '', tolower(expr))
	expr = gsub(' in every ', ' every ', expr)
	expr = gsub(' of every ', ' every ', expr)	
	tokens = trim(spl(spl(spl(expr, ' in '), ' every '), ' of '))
	
	stack = list(
		splits = date.all(dates), 
		dates.index = 1:len(dates)
	)
	selected = rep.row(c(1,len(dates)), len(dates))
		selected.n = 1
	
			
	# process in reverse order
	for(token in rev(tokens[nchar(tokens) > 0])) {
		selected0 = selected[1:selected.n, , drop=F]
			selected.n = 0
		for(i in 1:nrow(selected0)) {
			temp = custom.date.token(token, dates, stack, selected0[i,], keep.outside)
			if(nrow(temp) > 0) {
				selected[(selected.n+1):(selected.n+nrow(temp)),] = temp
				selected.n = selected.n + nrow(temp)
			} else return(c())
		}
	}
	selected[1:selected.n,1]
}


###############################################################################
# Helper functions to support custom date logic
#
# 1,2,3,4,5
# 5 = last day
# 4 = last but 1
# 	1st from/to last
# 	1 before last 
# 3 = last but 2
# 	2nd from/to last
# 	2 before last 
#
# Q | 1st Q | Apr | last day | 3rd Mon | 2nd from/to/before last Mon
###############################################################################
custom.date.token = function(expr, dates, stack, selected, keep.outside) {
	# split into tokens and remove empty ones
	tokens = trim(spl(tolower(expr), ' '))
		tokens = tokens[nchar(tokens) > 0]
		n.tokens = len(tokens)
		
	# last token is freq
	freq = custom.date.map(tokens[n.tokens])
	periods = date.periodicity.map(freq$freq)
		
	if( is.null(periods) )
		warning('unknown freq', freq$freq)

	if( periods == 'days' ) {
		temp = cbind(selected[1]:selected[2], selected[1]:selected[2])
			rownames(temp) =  stack$splits$dayofweek[selected[1]:selected[2]]
	} else
		temp = custom.date.extract(selected[1], selected[2], periods, stack) 
				
	if( !is.null(freq$pick) )
		temp = temp[rownames(temp) == freq$pick,,drop=F]

	# done
	if( n.tokens == 1 ) return(temp)
	
	# 1/2/3st | first | last | 1/2/3nd from/to/before last
	if( n.tokens == 2 ) {
		if( tokens[1] == 'last' )
			return(mlast(temp))
		if( tokens[1] == 'first' )
			return(temp[1,,drop=F])
	}
			
	# parse number
	offset = extract.pattern(tokens[1], '^[0-9]+')[1]
	if( is.na(offset) ) warning('unknown offset', tokens[1])
	offset = as.numeric(offset)
	
	if( offset > nrow(temp) ) {
		if(keep.outside) {
			if( n.tokens == 2 )
				mlast(temp)			
			else
				temp[1,,drop=F]
		}else
			matrix(NA,0,0)
	} else {
		if( n.tokens == 2 )		
			temp[offset,,drop=F]
		else
			temp[nrow(temp)-offset,,drop=F]
	}
}




extract.pattern = function(txt, pattern = '^[0-9]+') {
	pos = regexpr(pattern, txt)
	if(pos > 0) substr(txt, pos, pos+attr(pos,"match.length")-1)
	else NA
}
	

month.map.abbr = 1:12
	names(month.map.abbr) = spl('jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec')
month.map = 1:12
	names(month.map) = spl('january,february,march,april,may,june,july,august,september,october,november,december')

	
day.map.abbr = 0:6
	names(day.map.abbr) = spl('sun,mon,tue,wed,thu,fri,sat')
day.map = 0:6
	names(day.map) = spl('sunday,monday,tuesday,wednesday,thursday,friday,saturday')

	
custom.date.map = function(token) {
	if( !is.na(month.map.abbr[token]) )
		return(list(freq='months', pick = month.map.abbr[token]))
	if( !is.na(month.map[token]) )
		return(list(freq='months', pick = month.map[token]))

	if( !is.na(day.map.abbr[token]) )
		return(list(freq='day', pick = day.map.abbr[token]))
	if( !is.na(day.map[token]) )
		return(list(freq='day', pick = day.map[token]))
				
	#load.packages('stringr')
	# Q1	
	match = extract.pattern(token, '^q[1-4]{1}')[1]
	if( !is.na(match) ) return(list(freq='quarter', pick = substring(match,2,2) ))
	
	match = extract.pattern(token, '^m[0-9]+')[1]
	if( !is.na(match) ) return(list(freq='month', pick = substring(match,2,3) ))

	match = extract.pattern(token, '^s[1-2]{1}')[1]
	if( !is.na(match) ) return(list(freq='semiannual', pick = substring(match,2,2) ))	
	
	
	# probably year
	if(is.null(date.periodicity.map(token))) 
		list(freq='year', pick = token )
	else 
		list(freq=token)				
}


custom.date.extract = function(i0, i1, freq, stack) {
	label = stack$splits[[freq]][i0 : i1]
		label.n = len(label)
	
	temp = unique(c( 0, stack$dates.index[1:label.n][diff( label ) != 0], label.n ))
		temp.n = len(temp)
		
	temp = cbind(1 + temp[1:(temp.n - 1)], temp[2:temp.n])
		rownames(temp) =  label[temp[,1]]
	(i0 - 1) + temp
}


###############################################################################
# Tests for custom date logic
###############################################################################
custom.date.examples = function () {  
	# examples
	dates = date.remove.weekends(seq(Sys.Date()-1000, Sys.Date(), 1))
	dates[custom.date('last day in Apr', dates)]
	dates[custom.date('first day in Apr', dates)]
	dates[custom.date('last day in first week in Apr', dates)]

	# last
	dates[custom.date('last Mon in Apr', dates)]
	dates[custom.date('last Fri in Apr', dates)]

	# first
	dates[custom.date('first day in Apr', dates)]
	dates[custom.date('1st day in Apr', dates)]

	# offset
	dates[custom.date('10th day in Apr', dates)]
	dates[custom.date('50th day in Apr', dates)]

	# offset backwards
	dates[custom.date('10th to last day in Apr', dates)]

	# reference to Q/M
	dates[custom.date('3rd Mon in Q', dates)]
	dates[custom.date('3rd Mon in 1st Q', dates)]
	dates[custom.date('3rd Mon in Q1', dates)]
	dates[custom.date('3rd Mon in last M in Q1', dates)]

	# Options Expiration is third Friday of the expiration month
	# the expiration months are the first month of each quarter - January, April, July, October. 
	format(dates[custom.date('3rd Fri in Q', dates)], '%Y %b %d %w %a')
	format(dates[custom.date('last day in 3rd week in Q', dates)], '%Y %b %d %w %a')

	# Example for keep.outside flag
	format(dates[custom.date('5th Mon in Apr', dates)], '%Y %b %d %w %a')
	format(dates[custom.date('5th Mon in Apr', dates,FALSE)], '%Y %b %d %w %a')

}


