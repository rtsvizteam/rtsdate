

###############################################################################
#' Dates Functions
#'
#' @param dates collection of dates
#'
#' @return transformed dates
#'
#' @examples
#' \dontrun{ 
#' date.dayofweek(Sys.Date())
#' }
#' @export 
#' @rdname DateFunctions
###############################################################################
date.dayofweek <- function(dates) as.POSIXlt(dates)$wday


#' @export 
#' @rdname DateFunctions
date.day <- function(dates) as.POSIXlt(dates)$mday


# wday 0-6 day of the week, starting on Sunday.
# %U Week of the year as decimal number (00-53) using Sunday as the first day 1 of the week (and typically with the first Sunday of the year as day 1 of week 1). The US convention.
#' @export 
#' @rdname DateFunctions
date.week <- function(dates) { 
	dates = as.POSIXlt(dates)
	offset = (7 + dates$wday - dates$yday %% 7) %%7
	(dates$yday +  offset)%/% 7
}

 
#' @export 
#' @rdname DateFunctions
date.month <- function(dates) as.POSIXlt(dates)$mon + 1


quarter.map = c(1,1,1,2,2,2,3,3,3,4,4,4)

#' @export 
#' @rdname DateFunctions
date.quarter <- function(dates) quarter.map[date.month(dates)] 



semiannual.map = c(1,1,1,1,1,1,2,2,2,2,2,2)

#' @export 
#' @rdname DateFunctions
date.semiannual = function (dates) semiannual.map[date.month(dates)] 



#' @export 
#' @rdname DateFunctions
date.year = function (dates) as.POSIXlt(dates)$year + 1900

#' @export 
#' @rdname DateFunctions
date.all = function(dates) 
{
	dates = as.POSIXlt(dates)
	offset = (7 + dates$wday - dates$yday %% 7) %%7

	list(
		dayofweek = dates$wday,
		mday = dates$mday,
		yday = dates$yday,
		weeks = (dates$yday +  offset)%/% 7,
		months = dates$mon + 1,		
		quarters = quarter.map[dates$mon + 1],
		semiannual = semiannual.map[dates$mon + 1],
		years = dates$year + 1900	
	)
}


###############################################################################
#' Dates Index Functions
#'
#' @param dates collection of dates
#'
#' @return location of the week/month/year ends
#'
#' @examples
#' \dontrun{ 
#' date.week.ends(seq(Sys.Date()-100, Sys.Date(), 1))
#' }
#' @export 
#' @rdname DateFunctionsIndex
###############################################################################
date.week.ends <- function(dates) ends = which(diff( date.week(dates) ) != 0)


#' @export 
#' @rdname DateFunctionsIndex
date.month.ends <- function(dates) which(diff( 100*date.year(dates) + date.month(dates) ) != 0)


#' @export 
#' @rdname DateFunctionsIndex
date.quarter.ends <- function(dates) which(diff( 10*date.year(dates) + date.quarter(dates) ) != 0)

#' @export 
#' @rdname DateFunctionsIndex
date.semiannual.ends = function(dates)  which(diff( 10*date.year(dates) + date.semiannual(dates) ) != 0)


#' @export 
#' @rdname DateFunctionsIndex
date.year.ends <- function(dates) which(diff( date.year(dates) ) != 0)



###############################################################################
#' Dates Mapping Functions
#'
#' @param periodicity periodicity (i.e. weeks, months)
#'
#' @return standard periodicity
#'
#' @examples
#' \dontrun{ 
#' date.freq.map('week')
#' }
#' @export 
#' @rdname DateFunctionsMap
###############################################################################
date.periodicity.map = function(periodicity) {
  switch(periodicity,
	days = 'days',
    day = 'days',
    daily = 'days',
	d = 'days',
  
    weeks = 'weeks',
    week = 'weeks',
    weekly = 'weeks',
	w = 'weeks',
    
    months = 'months',
    month = 'months',
    monthly = 'months',
	m = 'months',
    
    quarters = 'quarters',
    quarter = 'quarters',
    quarterly = 'quarters',
	q = 'quarters',

	semiannual = 'semiannual',
	semiannually = 'semiannual',
	s = 'semiannual',
		
    years = 'years',
    year = 'years',
    yearly = 'years',
    annual = 'years',
    annually = 'years',
	y = 'years',
    
    # default
    NULL)  
}

#' @export 
#' @rdname DateFunctionsMap
date.ends.fn = function(periodicity) {
  switch(date.periodicity.map(periodicity),
    weeks = date.week.ends,
    months = date.month.ends,
    quarters = date.quarter.ends,
	semiannual = date.semiannual.ends,
    years = date.year.ends,
    
    # default
    NULL)  
}

###############################################################################
#' Remove Weekends from given collection of dates
#'
#' @param dates collection of dates
#' @param saturday code for Saturday, \strong{defaults to 6}
#' @param sunday code for Sunday, \strong{defaults to 0}
#'
#' @return dates with weekends removed
#'
#' @export 
###############################################################################
date.remove.weekends = function(dates, saturday = 6, sunday = 0) {
	wd = date.dayofweek(dates)
	rm.index = wd == saturday | wd == sunday
	dates[!rm.index]
}


