rtsdate
====


User Friendly Date Extraction 
===


Collection of functions to ease date specification and extraction.
For example, given the array of dates, find all 3rd Fridays in April with
with following syntax: 3rd Fri in Apr. Sometimes Friday can be a holiday,
so to get last date in the 3rd week in April, use following syntax:
last day in 3rd week in Apr. 


Installation:
===

```R
remotes::install_bitbucket('rtsvizteam/rtsdate')
```


Example : 
===

To run this example, please install with remotes:

```R
remotes::install_bitbucket('rtsvizteam/rtsdate')
```

```R
	library(rtsdate)
	
	dates = seq(as.Date('2015-12-31'), as.Date('2018-12-31'), 1)
	custom.date('last day in Apr', dates)	
	
	# helper function
	extract = function(expr) format(dates[custom.date(expr, dates)], '%a %d %b, %Y')
	
	# before
	extract('10th to last day in Apr')
	
	# after
	extract('10th day in Apr')
	
	# for set date: 10 Apr 2019, we need help to format date i.e.
	# 10 Apr 2019 -> 10 day in Apr in 2019
	extract('10 day in Apr in 2019')
	
	# add / remove syntax
	# use '+' to add dates
	# use '-' to remove dates
	extract('10th to last day in Apr + 10th day in Apr -10 day in Apr in 2019')
	
```	
